package lib

import (
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_indicator"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
	"gitlab.com/msex/srv_indicator/indicator"
	"gitlab.com/msex/srv_indicator/indicator/base"
)

func init() {
	indicator.Reg(new(Boll))
}

type Boll struct{}

func (b *Boll) Ty() m_indicator.IndType {
	return m_indicator.IndType_IT_Boll
}

func (b *Boll) Calc(periods []*m_period.Period, _ int32, custom ...string) (*m_indicator.Indicator, error) {
	period := util.ToInt32(custom[0])
	mid := base.MA(periods, period)
	std := base.STD(periods, period) * 2

	return &m_indicator.Indicator{Ind: &m_indicator.Indicator_Boll{Boll: &m_indicator.Boll{
		Up:   mid + std,
		Mid:  mid,
		Down: mid - std,
	}}}, nil
}
