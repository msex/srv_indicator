package base

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
)

func RMA(periods []*m_period.Period, n int32) float64 {
	alpha := float64(n)
	sum := MA(periods, n)
	for i := len(periods) - int(n) + 1; i < len(periods); i++ {
		sum = (periods[i].Close + (alpha-1)*sum) / alpha
	}

	return sum
}
