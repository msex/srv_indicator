package base

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
)

func EMA(items []*m_period.Period, n int32) float64 {
	ema := MA(items, n)
	k := 2.0 / float64(1.0+float64(n))
	for i := int(n); i < len(items); i++ {
		ema = (items[i].Close-ema)*k + ema
	}

	return ema
}
