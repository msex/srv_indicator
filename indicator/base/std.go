package base

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
	"math"
)

func STD(periods []*m_period.Period, n int32) float64 {
	return math.Sqrt(VARIANCE(periods, n))
}
