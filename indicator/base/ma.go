package base

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
)

func MA(periods []*m_period.Period, n int32) float64 {
	return SUM(periods, n) / float64(n)
}
