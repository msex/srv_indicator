package base

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
	"math"
)

func VARIANCE(periods []*m_period.Period, n int32) float64 {
	var res float64 = 0
	m := MA(periods, n)
	for i := len(periods) - int(n); i < len(periods); i++ {
		res += math.Pow(periods[i].Close-m, 2)
	}

	return res / float64(n)
}
