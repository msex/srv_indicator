package base

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
)

func SUM(periods []*m_period.Period, period int32) float64 {
	var res float64 = 0
	for i := len(periods) - int(period); i < len(periods); i++ {
		res += periods[i].Close
	}

	return res
}
