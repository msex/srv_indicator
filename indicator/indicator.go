package indicator

import (
	"errors"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_indicator"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
)

type IndImpl interface {
	Ty() m_indicator.IndType
	Calc([]*m_period.Period, int32, ...string) (*m_indicator.Indicator, error)
}

var (
	ins *Indicator
)

var (
	ErrUnknownInd = errors.New("unknown indicator")
)

func init() {
	ins = &Indicator{lib: make(map[m_indicator.IndType]IndImpl)}
	_ = comp.RegComp(ins)
}

type Indicator struct {
	lib map[m_indicator.IndType]IndImpl
}

func (i *Indicator) Init(map[string]interface{}, ...interface{}) error {

	return nil
}

func (i *Indicator) Start(...interface{}) error {
	return nil
}

func (i *Indicator) UnInit() {

}

func (i *Indicator) Name() string {
	return "indicator"
}

func Reg(impl IndImpl) {
	ins.Reg(impl)
}

func (i *Indicator) Reg(impl IndImpl) {
	i.lib[impl.Ty()] = impl
}

func Calc(ty m_indicator.IndType, items []*m_period.Period, period int32, custom ...string) (*m_indicator.Indicator, error) {
	return ins.Calc(ty, items, period, custom...)
}

func (i *Indicator) Calc(ty m_indicator.IndType, items []*m_period.Period, period int32, custom ...string) (*m_indicator.Indicator, error) {
	ind, ok := i.lib[ty]
	if !ok {
		return nil, ErrUnknownInd
	}

	return ind.Calc(items, period, custom...)
}
