package main

import (
	"context"
	"gitlab.com/flex_comp/log"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_indicator"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"gitlab.com/msex/model_pb/kitex_gen/srv/s_indicator"
	"gitlab.com/msex/srv_indicator/service"
	"testing"
	"time"
)

func Test_indicator(t *testing.T) {
	impl := new(service.IndicatorImpl)

	t.Run("indicator", func(t *testing.T) {
		go run()
		<-time.After(time.Second)

		rsp, e := impl.Request(context.TODO(), &s_indicator.ReqIndicator{
			Ex:     m_common.Exchange_EX_Binance,
			ExType: m_common.ExchangeType_ET_Spot,
			Symbol: &m_symbol.Symbol{
				Base:  "btc",
				Quote: "usdt",
			},
			Ty:          m_indicator.IndType_IT_Boll,
			PeriodType:  m_period.PeriodType_PT_1min,
			SampleCount: 300,
			Custom:      []string{"20"},
		})

		if e != nil {
			t.Fatal(e)
		}

		log.Info(rsp.Ind.Ind)
	})
}
