package main

import (
	"fmt"
	"github.com/cloudwego/kitex/server"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/kitex"
	"gitlab.com/flex_comp/log"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/srv/s_indicator/indicator"
	"gitlab.com/msex/srv_indicator/service"
	"os"
	"strings"
)

func main() {
	run()
}

func run() {
	log.InfoF("启动服务, 版本: %s, 构建时间(UTC): %s", define.Version, define.BuildDate)
	kitex.RegService(define.SrvIndicator(), func(option ...server.Option) server.Server {
		return indicator.NewServer(new(service.IndicatorImpl), option...)
	})

	env := os.Getenv(define.EnvSrvEnv)
	if env != define.EnvProd {
		env = define.EnvDebug
	}

	args := make(map[string]interface{})
	args["env"] = env
	args["etcd_hosts"] = strings.Split(os.Getenv(define.EnvEtcdHosts), ",")
	args["log.path"] = fmt.Sprintf("%s/%s", define.LogDir, "s")
	args["log.size"] = define.LogSize
	args["log.age"] = define.LogAge

	err := comp.Init(args)
	if err != nil {
		log.Error("组件初始化失败:", err)
		return
	}

	err = comp.Start(nil)
	if err != nil {
		log.Error("组件启动失败:", err)
		return
	}
}
