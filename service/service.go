package service

import (
	"context"
	"gitlab.com/flex_comp/kitex"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_period"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_quote"
	"gitlab.com/msex/model_pb/kitex_gen/srv/s_indicator"
	"gitlab.com/msex/model_pb/kitex_gen/srv/s_quote"
	"gitlab.com/msex/model_pb/kitex_gen/srv/s_quote/quote"
	"gitlab.com/msex/srv_indicator/indicator"
	_ "gitlab.com/msex/srv_indicator/indicator/lib"
	"math"
	"sync"
	"time"
)

type IndicatorImpl struct{}

func open(pt m_period.PeriodType, cur time.Time) time.Time {
	cur = cur.UTC()
	switch pt {
	case m_period.PeriodType_PT_1min:
		return cur.Truncate(time.Minute)
	case m_period.PeriodType_PT_5min:
		return cur.Truncate(time.Minute * 5)
	case m_period.PeriodType_PT_15min:
		return cur.Truncate(time.Minute * 15)
	case m_period.PeriodType_PT_30min:
		return cur.Truncate(time.Minute * 30)
	case m_period.PeriodType_PT_1hour:
		return cur.Truncate(time.Hour)
	case m_period.PeriodType_PT_4hour:
		return cur.Truncate(time.Hour * 4)
	case m_period.PeriodType_PT_1day:
		return cur.Truncate(time.Hour * 24)
	case m_period.PeriodType_PT_1week:
		offset := int(time.Monday - cur.Weekday())
		if offset > 0 {
			offset = -6
		}
		t := time.Date(cur.Year(), cur.Month(), cur.Day(), 0, 0, 0, 0, cur.Location())
		t = t.AddDate(0, 0, offset)
		return t
	case m_period.PeriodType_PT_1mon:
		return time.Date(cur.Year(), cur.Month(), 1, 0, 0, 0, 0, cur.Location())
	}

	return time.Time{}
}

func updatePeriod(pt m_period.PeriodType, p []*m_period.Period, q *m_quote.Quote) []*m_period.Period {
	var (
		newPeriod bool
		curOpen   time.Time
	)

	for b := true; !b; b = false {
		curOpen = open(pt, time.UnixMilli(q.Ts))
		if len(p) == 0 {
			newPeriod = true
			break
		}

		latest := p[len(p)-1]
		if curOpen.UnixMilli() != latest.OpenTm {
			newPeriod = true
			break
		}

		latest.CloseTm = q.Ts
		latest.Close = q.Price
		latest.Max = math.Max(latest.Max, q.Price)
		latest.Min = math.Min(latest.Min, q.Price)
	}

	if !newPeriod {
		return p
	}

	return append(p, &m_period.Period{
		OpenTm:  curOpen.UnixMilli(),
		CloseTm: q.Ts,
		Open:    q.Price,
		Close:   q.Price,
		Max:     q.Price,
		Min:     q.Price,
	})
}

func (s *IndicatorImpl) Stream(req *s_indicator.ReqIndicator, stream s_indicator.Indicator_StreamServer) (err error) {
	chQuote, quoteWId := define.QuoteStreamWatch(req.Ex, req.ExType, req.Symbol)
	defer func() {
		define.QuoteStreamUnWatch(req.Ex, req.ExType, req.Symbol, quoteWId)
	}()

	var (
		period []*m_period.Period
		init   sync.Once
	)

	for m := range chQuote {
		// 收到行情后请求周期数据,避免中间遗漏
		init.Do(func() {
			o, e := quote.NewClient(define.CliQuote(req.Ex), kitex.CliOpts()...)
			if e != nil {
				err = e
				return
			}

			r, e := o.History(context.TODO(), &s_quote.ReqHistory{
				Ex:          req.Ex,
				ExType:      req.ExType,
				Symbol:      req.Symbol,
				PeriodType:  req.PeriodType,
				SampleCount: req.SampleCount,
			})

			period = r.Sample
		})

		q := m.(*s_quote.RspQuote)
		period = updatePeriod(req.PeriodType, period, q.Quote)

		ind, e := indicator.Calc(req.Ty, period, req.SampleCount, req.Custom...)
		if err != nil {
			err = e
			return
		}

		if err = stream.Send(&s_indicator.RspIndicator{Ind: ind}); err != nil {
			return
		}
	}

	return
}

func (s *IndicatorImpl) Request(ctx context.Context, req *s_indicator.ReqIndicator) (res *s_indicator.RspIndicator, err error) {
	o, e := quote.NewClient(define.CliQuote(req.Ex), kitex.CliOpts()...)
	if e != nil {
		err = e
		return
	}

	r, e := o.History(context.TODO(), &s_quote.ReqHistory{
		Ex:          req.Ex,
		ExType:      req.ExType,
		Symbol:      req.Symbol,
		PeriodType:  req.PeriodType,
		SampleCount: req.SampleCount,
	})

	if e != nil {
		err = e
		return
	}

	ind, e := indicator.Calc(req.Ty, r.Sample, req.SampleCount, req.Custom...)
	if e != nil {
		err = e
		return
	}

	return &s_indicator.RspIndicator{Ind: ind}, nil
}
